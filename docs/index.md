# Space Invader

Projet du second semestre de :

* [Léopold Clément](mailto:leopold.clement@ens-paris-saclay.fr)

* [Thomas Omarini](mailto:thomas.omarini@ens-paris-saclay.fr)

Si vous avez des difficultés avec le jeu, un tutoriel est disponible [ici](tuto.md).

Sinon, tous les détails techniques sont expliqués [ici](archi.md).

!!! info "Record de fin"
    Si vous arrivez jusqu'à la vague 15, prévenez nous.

Nous remercions [Maïna Garnero](mailto:maina.garnero@ens-paris-saclay.fr), biologiste de son état, pour sa participation à la réalisation de tous les éléments graphiques du jeu.
