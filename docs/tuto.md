# Tutoriel 

## Contrôles

Le vaisseau se déplace avec le joystick. Pour utiliser le tir principal, il faut presser le bouton ```BP1``` (en bas). On dispose d'une seconde attaque, lançant une multitude de missiles avec le bouton ```BP2```(en haut).

L'attaque secondaire a besoin de se recharger pendant quelques secondes, le taux de charge est modélisé par les leds à droite de la carte.

## Mécanique
Le but est de détruire toutes les créatures ennemies. Une fois tous les monstres détruits, une nouvelle vague apparaît. Chaque vague est plus dure que la précédante (les ennemis tirent plus de projectiles).

Vous ne pouvez prendre que 5 dégats avant la fin de la partie.

!!! hint
    Le tir secondaire est très puissant, il ne faut pas hésiter à l'utiliser. De plus, il faut se consentrer sur l'évitement des projectiles ennemis.
## Fin
Le jeu prend fin lorsque vous n'avez plus de vie. 


